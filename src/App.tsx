import React from "react";
//import logo from "./logo.svg";
import "./App.scss";
import Login from "./components/Login/Login";
import { Route, Switch } from "react-router";
import Profile from "./components/newPage/Profile";
import Display from "./components/newPage/StudentActivity";
import DisplayTable from "./components/newPage/CampusActivity";
import activityDetailPage from "./components/newPage/activityDetailPage";
import EditStudent from "./components/newPage/EditStudent";
//import Login from "./components/Login/Login";

//import Table from './components/newPage/newTable'

function App() {
  return (
    <>
      <Switch>
        <Route path="/" exact component={Login} />
        <Route
          path="/detailActivity/:companyId"
          exact
          component={activityDetailPage}
        />

        <Route path="/display" exact component={DisplayTable} />
        <Route path="/editAction/:companyId" component={EditStudent} />
        <Route path="/display2" exact component={Display} />
      </Switch>
    </>
  );
}

export default App;
