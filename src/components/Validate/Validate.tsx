import { RepeatOneSharp } from "@mui/icons-material";
import axios from "axios";

interface IForm {
  userName: string;
  password: string;
}

const ValidateUser = async (formData: IForm) => {
  console.log(formData);
  const response = await axios.post("/admin/login", formData);
  console.log(">>>sgh", response.data.err);
  console.log("response>>>>", response.data.result, "<<<<", response);

  if (response.data.result) {
    localStorage.setItem("token", response.data.result.token);
    return response.data.result.token;
  }
  // localStorage.removeItem("token");
  return null;
};
export default ValidateUser;
