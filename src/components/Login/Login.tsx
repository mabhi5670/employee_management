import styles from "./Login.module.scss";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { IForm } from "./Login.types";
import { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import ValidateUser from "../Validate/Validate";
import { error } from "console";

const schema = yup.object().shape({
  userName: yup
    .string()
    .required("Please Enter your email")
    .matches(/^[^\s@]+@[^\s@]+\.[^\s@]+$/),

  password: yup
    .string()
    .required("Please Enter your password")
    .matches(
      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
      "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
    ),
});

interface Props {
  setIsAuth: React.Dispatch<React.SetStateAction<boolean>>;
}
const Login = () => {
  const history = useHistory();
  const [error, setError] = useState(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IForm>({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (response: any) => {
    const result = await ValidateUser(response);
    console.log(">>>result", result);
    if (result) {
      setError(false);

      history.push("/display");
    } else {
      alert("password is not correct");
      //  setError(true);
      //  error&&<h1>details are wrong</h1>
    }
  };

  //  const onSubmit = (data: IForm) =>{
  //    const result = true
  //    if(result){
  //                   setError(false);
  //                   history.push("/display");

  //    }
  //    else{
  //      setError(true)
  //    }
  //  }

  const handleclick = () => {
    console.log("onclick");
  };

  return (
    <>
      <div className={styles.container} onClick={handleclick}>
        <div className={styles.top}></div>
        <div className={styles.bottom}></div>
        <div className={styles.center}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <h2>Please Sign In</h2>
            <input type="email" placeholder="email" {...register("userName")} />
            <p> {errors.userName?.message}</p>
            <input
              type="password"
              placeholder="password"
              {...register("password")}
            />
            {errors.password && <p>{errors.password.message}</p>}
            <button>Login</button>

            <Link to="/home" className={styles.forgotPassword}>
              Forgot Password?
            </Link>
          </form>
        </div>
      </div>
    </>
  );
};

export default Login;
