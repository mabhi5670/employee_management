import { Link, Route,Switch } from "react-router-dom";
import Profile from "../newPage/Profile"



const Dashboard = () => {
    return(
        <>
        <section>
        <Link to="/dashboard/profile">Profile</Link>
        </section>


        <main>
            <Switch>
                <Route path="/dashboard/profile" component={Profile}/>
            </Switch>
        </main>
        </>

    )
}

export default Dashboard;