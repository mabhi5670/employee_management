import * as React from "react";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { useState } from "react";
import Button from "@mui/material/Button";
import axios from "axios";
import styles from "./StudentActivity.module.scss";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function TransitionsModal({
  open,
  handleClose,
  setTableData,
  tabledata,
}: any) {
  const [value, setValue] = React.useState("");

  const handleChange = (event: SelectChangeEvent) => {
    setValue(event.target.value as string);
  };
  const [studentData, setStudentData] = useState([]);
  const token = localStorage.getItem("token");

  React.useEffect(() => {
    const getData = async () => {
      const { data } = await axios.get(`/student`, {
        headers: {
          Authorization: token,
        },
      });
      console.log("data>>>", data);
      if (data.result) {
        setStudentData(data.result);
      }
    };
    getData();
  }, []);
  console.log("studentdata>>>", studentData);

  const handleClick = () => {
    const data = studentData.filter((student: any) => student.id === value);
    console.log("handleClickdata>>>>", data);
    // const newData = {data[0].student}
    setTableData([...tabledata, data[0]]);
    handleClose();
  };

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label">Age</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={value}
                label="Name"
                onChange={handleChange}
              >
                {studentData.map(({ id, student }: any) => (
                  <MenuItem value={id}>{student}</MenuItem>
                ))}
              </Select>
            </FormControl>
            <Button onClick={handleClick}>ADD Student</Button>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
}
