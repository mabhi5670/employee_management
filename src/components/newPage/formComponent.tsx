import { campusData } from '../Table/TableData';
import EnhancedTable from './Profile';
import { StudentDetails } from './Tableobj';
import styles from './StudentActivity.module.scss';
import axios from 'axios';
import { useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import TransitionsModal from './Modal';
import { TextField } from '@mui/material';
import { useHistory, useParams } from 'react-router-dom';

interface IForm {
  company: string;
  noOfStudents: string;
  noOfVacancies: string;
  domain: string;
}
const token = localStorage.getItem('token');
const schema = yup.object().shape({
  company: yup.string(),
  noOfStudents: yup.string(),
  noOfVacancies: yup.string(),
  domain: yup.string(),
});
const FormData = ({ formData, tabledata, id }: any) => {
  const { register, handleSubmit } = useForm<IForm>({
    resolver: yupResolver(schema),
    defaultValues: formData,
  });
  const history = useHistory();
  const onSubmit = async (response: any) => {
    const newTableData = tabledata.map(async (student: any) => {
      const { id: sID, ...newStudent } = student;
      const studentId = { CampusActivityId: id, studentDetailId: sID };
      const data = await axios.post(`/activityDetailPage/`, studentId, {
        headers: {
          Authorization: token,
        },
      });
      console.log('mydataaaa>><<<', data);
      return newStudent;
    });
    // const newData = { ...response, students: newTableData };
    // console.log('newdata>>>>', newData);

    const data = await axios.put(`/campusactivity/edit/${id}`, response, {
      headers: {
        Authorization: token,
      },
    });

    console.log(data, '>>>data');
    console.log('tabledata>>><<<<', tabledata);
    history.push('/display');
  };
  console.log(onSubmit, '...submit');

  return (
    <>
      <span>
        <h2>Student Details</h2>
      </span>

      <Box
        component='form'
        sx={{
          '& .MuiTextField-root': { m: 1, width: '25ch' },
        }}
        noValidate
        autoComplete='off'
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className={styles.Input1}>
          <TextField
            id='outlined-company-input'
            label='Company'
            type='text'
            autoComplete='current-company'
            {...register('company')}
          />

          <TextField
            id='outlined-domain-input'
            label='Domain'
            type='text'
            autoComplete='current-domain'
            {...register('domain')}
          />

          <TextField
            id='outlined-noOfStudents-input'
            label='No.Of Students'
            type='text'
            autoComplete='current-noOfStudents'
            {...register('noOfStudents')}
          />

          <TextField
            id='outlined-noOfVacancies-input'
            label='No.of vacancies'
            type='text'
            autoComplete='current-noOfVacancies'
            {...register('noOfVacancies')}
          />

          <button type='submit' className={styles.btn}>
            Save
          </button>
        </div>
      </Box>
    </>
  );
};

export default FormData;
