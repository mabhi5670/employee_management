import { campusData } from '../Table/TableData';
import EnhancedTable from './Profile';
import { StudentDetails } from './Tableobj';
import styles from './StudentActivity.module.scss';
import axios from 'axios';
import { useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import TransitionsModal from './Modal';
import { TextField } from '@mui/material';
import { useHistory, useParams } from 'react-router-dom';
import FormData from './FormComponent';
interface IParam {
  companyId: string;
}

const EditStudent = () => {
  //Modal code
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  //Delete Students
  const deleteStudents = async (id: any) => {
    const { data } = await axios.delete(`/student/${id}`);
    if (data.result) {
      alert('Student deleted successfully');
    }
  };

  // API request code
  console.log(campusData);
  const token = localStorage.getItem('token');
  const [tabledata, setTableData] = useState([]);
  const { companyId } = useParams<IParam>();
  const [formData, setFormData] = useState({
    company: '',
    noOfStudents: '',
    noOfVacancies: '',
    domain: '',
  });
  useEffect(() => {
    const getData = async () => {
      const { data } = await axios.get(`/campusactivity/detail/${companyId}`, {
        headers: {
          Authorization: token,
        },
      });
      console.log('data>>>', data);
      if (data.result) {
        setFormData({
          company: data.result.company,
          noOfStudents: data.result.noOfStudents,
          noOfVacancies: data.result.noOfVacancies,
          domain: data.result.domain,
        });
        setTableData(data.result['student-Details']);
        console.log(setFormData, 'setFormData///////');
      }
    };
    getData();
  }, []);
  // const history = useHistory();

  //ADDING NEW DATA TO THE CAMPUS TABLE..................................................

  // interface IForm {
  //   company: string;
  //   noOfStudents: string;
  //   noOfVacancies: string;
  //   domain: string;
  // }

  // const schema = yup.object().shape({
  //   // company": "HCL",
  //   //     "domain": "Developer",
  //   //     "noOfStudents": 10,
  //   //     "noOfVacancies": 3,
  //   //     "student-Details
  //   company: yup.string(),
  //   noOfStudents: yup.string(),
  //   noOfVacancies: yup.string(),
  //   domain: yup.string(),
  // });
  // const { register, handleSubmit } = useForm<IForm>({
  //   resolver: yupResolver(schema),
  //   defaultValues: formData,
  // });
  // const history = useHistory();
  // const onSubmit = async (response: any) => {
  //   const newTableData = tabledata.map((student: any) => {
  //     const { id, ...newStudent } = student;
  //     return newStudent;
  //   });
  //   const newData = { ...response, students: newTableData };
  //   console.log("newdata>>>>", newData);
  //   const data = await axios.post(`/campusactivity/addnew`, newData, {
  //     headers: {
  //       Authorization: token,
  //     },
  //   });
  //   console.log(data, ">>>data");
  //   history.push("/display");
  // };
  // console.log(onSubmit, "...submit");

  return (
    <>
      {formData.company && (
        <FormData formData={formData} tabledata={tabledata} id={companyId} />
      )}

      {/* <span>
        <h2>Student Details</h2> */}
      {/* </span>
      <div className={styles.container}> */}
      {/* {formData.domain ? (
          <Box
            component="form"
            sx={{
              "& .MuiTextField-root": { m: 1, width: "25ch" },
            }}
            noValidate
            autoComplete="off"
            onSubmit={handleSubmit(onSubmit)}
          >
            <div className={styles.Input1}>
              <TextField
                id="outlined-company-input"
                label="Company"
                type="text"
                autoComplete="current-company"
                {...register("company")}
              />

              <TextField
                id="outlined-domain-input"
                label="Domain"
                type="text"
                autoComplete="current-domain"
                {...register("domain")}
              />

              <TextField
                id="outlined-noOfStudents-input"
                label="No.Of Students"
                type="text"
                autoComplete="current-noOfStudents"
                {...register("noOfStudents")}
              />

              <TextField
                id="outlined-noOfVacancies-input"
                label="No.of vacancies"
                type="text"
                autoComplete="current-noOfVacancies"
                {...register("noOfVacancies")}
              />

              <button type="submit" className={styles.btn}>
                Save
              </button>
            </div>
          </Box>
        ) : null} */}

      <div>
        <Button className={styles.btn1} onClick={handleOpen}>
          ADD Student
        </Button>
      </div>

      <div className={styles.table}>
        <EnhancedTable
          rows={tabledata}
          tableHeader={StudentDetails}
          deleteAction={deleteStudents}
          isDownloadBtn={true}
          firstKey='student'
          dataKeys={['year', 'Avg_Percent']}
        />
      </div>

      <TransitionsModal
        open={open}
        handleClose={handleClose}
        tabledata={tabledata}
        setTableData={setTableData}
      />
      {/* </div> */}
    </>
  );
};
export default EditStudent;
