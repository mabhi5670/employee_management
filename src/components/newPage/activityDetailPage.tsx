import { StudentTableData } from "../Table/StudentTableData";
import { campusData } from "../Table/TableData";
import EnhancedTable from "./Profile";
import { headCells, StudentDetails } from "./Tableobj";
import styles from "./StudentActivity.module.scss";
import * as React from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import axios from "axios";
import { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
interface IParam {
  companyId: string;
}
const ActivityDetailPage = () => {
  const history = useHistory();
  const { companyId } = useParams<IParam>();
  console.log("companyId>>>", companyId);
  const token = localStorage.getItem("token");
  const [tabledata, setTableData] = useState([]);
  useEffect(() => {
    const getData = async () => {
      const { data } = await axios.get(`/campusactivity/detail/${companyId}`, {
        headers: {
          Authorization: token,
        },
      });
      console.log("data>>>", data);
      if (data.result) {
        setTableData(data.result["student-Details"]);
      }
    };
    getData();
  }, []);
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <h1>ActivityDetail Page</h1>
      </div>

      <div className={styles.table}>
        <EnhancedTable
          className={styles.table1}
          rows={tabledata}
          tableHeader={StudentDetails}
          detailAction="detailAction"
          isDownloadBtn={true}
          firstKey="student"
          dataKeys={["year", "Avg_Percent"]}
        />
      </div>
    </div>
  );
};
export default ActivityDetailPage;
