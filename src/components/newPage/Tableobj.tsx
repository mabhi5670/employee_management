import {
  Box,
  Checkbox,
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
} from '@mui/material';
import {
  Data,
  EnhancedTableProps,
  HeadCell,
  StudentData,
} from '../Table.types';
import { visuallyHidden } from '@mui/utils';

export const headCells: readonly HeadCell[] = [
  {
    id: 'name',
    numeric: false,
    disablePadding: true,
    label: 'Company',
  },
  {
    id: 'domain',
    numeric: true,
    disablePadding: false,
    label: 'Domain',
  },
  {
    id: 'no_of_students',
    numeric: true,
    disablePadding: false,
    label: 'No. Of Students',
  },
  {
    id: 'no_Of_Vacancies',
    numeric: true,
    disablePadding: false,
    label: 'No. Of Vacancies',
  },
  {
    id: 'action',
    numeric: true,
    disablePadding: false,
    label: 'Actions',
  },
];

export const StudentDetails: readonly HeadCell[] = [
  {
    id: 'student',
    numeric: false,
    disablePadding: true,
    label: 'Student',
  },
  {
    id: 'year',
    numeric: true,
    disablePadding: false,
    label: 'Year',
  },
  {
    id: 'avgPercentage',
    numeric: true,
    disablePadding: false,
    label: 'Average Percentage',
  },
  {
    id: 'cv',
    numeric: true,
    disablePadding: false,
    label: 'CV',
  },
  {
    id: 'action',
    numeric: true,
    disablePadding: false,
    label: 'Action',
  },
];

export default function EnhancedTableHead(props: EnhancedTableProps) {
  const {
    onSelectAllClick,
    order,
    orderBy,
    numSelected,
    rowCount,
    onRequestSort,
    tableHeader,
  } = props;
  const createSortHandler =
    (property: keyof Data | keyof StudentData) =>
    (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding='checkbox'>
          <Checkbox
            color='primary'
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{
              'aria-label': 'select all desserts',
            }}
          />
        </TableCell>
        {tableHeader.map((headCell: any) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component='span' sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}
