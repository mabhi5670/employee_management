import { StudentTableData } from "../Table/StudentTableData";
import { campusData } from "../Table/TableData";
import EnhancedTable from "./Profile";
import { headCells, StudentDetails } from "./Tableobj";
import AddIcon from "@mui/icons-material/Add";
import styles from "../newPage/CampusActivity.module.scss";
import { Redirect, useHistory } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";

const DisplayTable = () => {
  const history = useHistory();
  console.log(campusData);
  const token = localStorage.getItem("token");
  const [tabledata, setTableData] = useState([]);
  useEffect(() => {
    const getData = async () => {
      const { data } = await axios.get("/campusactivity/", {
        headers: {
          Authorization: token,
        },
      });
      console.log("data>>>", data);
      if (data.result) {
        setTableData(data.result);
      }
    };
    getData();
  }, []);
  if (!token) {
    return <Redirect to="/" />;
  }
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <h1>Campus Activity</h1>
        <button
          className={styles.btn}
          onClick={() => {
            history.push("/display2");
          }}
        >
          ADD <AddIcon className={styles.add} />
        </button>
      </div>

      <div className={styles.table}>
        <EnhancedTable
          className={styles.table1}
          rows={tabledata}
          tableHeader={headCells}
          editAction="editAction"
          detailAction="detailActivity"
          firstKey="company"
          dataKeys={["domain", "noOfStudents", "noOfVacancies"]}
        />
      </div>
    </div>
  );
};
export default DisplayTable;
