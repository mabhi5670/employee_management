import { StylesProvider } from "@material-ui/styles";
import { StudentTableData } from "../Table/StudentTableData";
import { campusData } from "../Table/TableData";
import EnhancedTable from "./Profile";
import { headCells, StudentDetails } from "./Tableobj";
import styles from "./StudentActivity.module.scss";
import * as React from "react";
import TextField from "@mui/material/TextField";
import axios from "axios";
import { Redirect, useHistory } from "react-router-dom";
import { useState, useEffect } from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import TransitionsModal from "./Modal";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};
const DisplayTable = () => {
  //Modal code
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  // API request code
  console.log(campusData);
  const token = localStorage.getItem("token");
  const [tabledata, setTableData] = useState([]);
  useEffect(() => {
    const getData = async () => {
      const { data } = await axios.get(`student/detail/`, {
        headers: {
          Authorization: token,
        },
      });
      console.log("data>>>", data);
      if (data.result) {
        setTableData(data.result);
      }
    };
    getData();
  }, []);
  // const history = useHistory();

  //ADDING NEW DATA TO THE CAMPUS TABLE..................................................

  interface IForm {
    company: string;
    noOfStudents: string;
    noOfVacancies: string;
    domain: string;
  }

  const schema = yup.object().shape({
    // company": "HCL",
    //     "domain": "Developer",
    //     "noOfStudents": 10,
    //     "noOfVacancies": 3,
    //     "student-Details
    company: yup.string(),
    noOfStudents: yup.string(),
    noOfVacancies: yup.string(),
    domain: yup.string(),
  });
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IForm>({
    resolver: yupResolver(schema),
  });
  const history = useHistory();
  const onSubmit = async (response: any) => {
    const newTableData = tabledata.map((student: any) => {
      const { id, ...newStudent } = student;
      return newStudent;
    });
    const newData = { ...response, students: newTableData };
    console.log("newdata>>>>", newData);
    const data = await axios.post(`/campusactivity/addnew`, newData, {
      headers: {
        Authorization: token,
      },
    });
    console.log(data, ">>>data");
    history.push("/display");
  };
  console.log(onSubmit, "...submit");

  return (
    <>
      <span>
        <h2>Student Details</h2>
      </span>
      <div className={styles.container}>
        <Box
          component="form"
          sx={{
            "& .MuiTextField-root": { m: 1, width: "25ch" },
          }}
          noValidate
          autoComplete="off"
          onSubmit={handleSubmit(onSubmit)}
        >
          <div className={styles.Input1}>
            <TextField
              id="outlined-company-input"
              label="Company"
              type="text"
              autoComplete="current-company"
              {...register("company")}
            />

            <TextField
              id="outlined-domain-input"
              label="Domain"
              type="text"
              autoComplete="current-domain"
              {...register("domain")}
            />

            <TextField
              id="outlined-noOfStudents-input"
              label="No.Of Students"
              type="text"
              autoComplete="current-noOfStudents"
              {...register("noOfStudents")}
            />

            <TextField
              id="outlined-noOfVacancies-input"
              label="No.of vacancies"
              type="text"
              autoComplete="current-noOfVacancies"
              {...register("noOfVacancies")}
            />

            <button type="submit" className={styles.btn}>
              Save
            </button>
          </div>
        </Box>

        <div>
          <Button className={styles.btn1} onClick={handleOpen}>
            ADD Student
          </Button>
        </div>

        <div className={styles.table}>
          <EnhancedTable
            rows={tabledata}
            tableHeader={StudentDetails}
            deleteAction="delete"
            isDownloadBtn={true}
            firstKey="student"
            dataKeys={["year", "Avg_Percent"]}
          />
        </div>

        <TransitionsModal
          open={open}
          handleClose={handleClose}
          tabledata={tabledata}
          setTableData={setTableData}
        />
      </div>
    </>
  );
};
export default DisplayTable;
