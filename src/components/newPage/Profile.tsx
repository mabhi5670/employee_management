import {
  alpha,
  Checkbox,
  FormControlLabel,
  IconButton,
  Paper,
  Switch,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableRow,
  Toolbar,
  Tooltip,
  Typography,
} from '@mui/material';
import {
  Data,
  EnhancedTableToolbarProps,
  Order,
  StudentData,
} from '../Table.types';
import EditIcon from '@mui/icons-material/Edit';
import { getComparator, stableSort } from './newTable';
import DeleteIcon from '@mui/icons-material/Delete';
import FilterListIcon from '@mui/icons-material/FilterList';
import React from 'react';
import EnhancedTableHead from './Tableobj';
import ButtonGroup from '@mui/material/ButtonGroup';
import Button from '@mui/material/Button';
import styles from './Profile.module.scss';
import Icon from '@mui/material/Icon';

import VisibilityIcon from '@mui/icons-material/Visibility';
import Box from '@mui/material/Box';
import Fab from '@mui/material/Fab';
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';
import { useHistory } from 'react-router';

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
  const { numSelected } = props;

  return (
    <Toolbar
      sx={{
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 },
        ...(numSelected > 0 && {
          bgcolor: (theme) =>
            alpha(
              theme.palette.primary.main,
              theme.palette.action.activatedOpacity
            ),
        }),
      }}
    >
      {numSelected > 0 ? (
        <Typography
          sx={{ flex: '1 1 100%' }}
          color='inherit'
          variant='subtitle1'
          component='div'
        >
          {numSelected} selected
        </Typography>
      ) : (
        <Typography
          sx={{ flex: '1 1 100%' }}
          variant='h6'
          id='tableTitle'
          component='div'
        >
          {/* Nutrition tag  */}
        </Typography>
      )}
      {numSelected > 0 ? (
        <Tooltip title='Delete'>
          <IconButton>
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title='Filter list'>
          <IconButton>
            <FilterListIcon />
          </IconButton>
        </Tooltip>
      )}
    </Toolbar>
  );
};

function EnhancedTable({
  rows,
  tableHeader,
  columnBy,
  editAction,
  detailAction,
  deleteAction,
  isDownloadBtn,
  firstKey,
  dataKeys,
}: any) {
  const [order, setOrder] = React.useState<Order>('asc');
  const [orderBy, setOrderBy] = React.useState<keyof Data | keyof StudentData>(
    columnBy
  );
  const [selected, setSelected] = React.useState<readonly string[]>([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data | keyof StudentData
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n: any) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected: readonly string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeDense = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDense(event.target.checked);
  };

  const isSelected = (name: string) => selected.indexOf(name) !== -1;

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;
  console.log(rows);
  const history = useHistory();
  return (
    <div className={styles.backgroundImg}>
      <div className={styles.main}>
        <main>
          <Box sx={{ width: '100%' }}>
            <Paper sx={{ width: '100%', mb: 2 }}>
              <EnhancedTableToolbar numSelected={selected.length} />
              <TableContainer>
                <Table
                  sx={{ minWidth: 750 }}
                  aria-labelledby='tableTitle'
                  size={dense ? 'small' : 'medium'}
                >
                  <EnhancedTableHead
                    numSelected={selected.length}
                    order={order}
                    orderBy={orderBy}
                    onSelectAllClick={handleSelectAllClick}
                    onRequestSort={handleRequestSort}
                    rowCount={rows.length}
                    tableHeader={tableHeader}
                  />
                  <TableBody className={styles.domain}>
                    {/* if you don't need to support IE11, you can replace the `stableSort` call with:
                rows.slice().sort(getComparator(order, orderBy)) */}
                    {stableSort(rows, getComparator(order, orderBy))
                      .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                      .map((row: any, index) => {
                        const isItemSelected = isSelected(row.name);
                        const labelId = `enhanced-table-checkbox-${index}`;

                        return (
                          <TableRow
                            className={styles.tableRow}
                            hover
                            onClick={(event) => handleClick(event, row.name)}
                            role='checkbox'
                            aria-checked={isItemSelected}
                            tabIndex={-1}
                            key={row.name}
                            selected={isItemSelected}
                          >
                            <TableCell padding='checkbox'>
                              <Checkbox
                                color='primary'
                                checked={isItemSelected}
                                inputProps={{
                                  'aria-labelledby': labelId,
                                }}
                              />
                            </TableCell>
                            <TableCell
                              component='th'
                              id={labelId}
                              scope='row'
                              padding='none'
                            >
                              {row[firstKey]}
                            </TableCell>
                            {dataKeys.map((key: any) => (
                              <TableCell align='right'>{row[key]}</TableCell>
                            ))}

                            {isDownloadBtn && (
                              <TableCell key={index} align='right'>
                                <a
                                  href={row.url}
                                  className={styles.download_btn}
                                  download
                                  onClick={(e) => e.stopPropagation()}
                                >
                                  Download
                                </a>
                              </TableCell>
                            )}
                            <TableCell align='right'>
                              <div className={styles.btnContainer}>
                                {editAction && (
                                  <button
                                    className={styles.edit_btn}
                                    onClick={(e) => {
                                      e.stopPropagation();
                                      history.push(`/${editAction}/${row.id}`);
                                    }}
                                  >
                                    <EditIcon />
                                  </button>
                                )}

                                {detailAction && (
                                  <button
                                    className={styles.detail_btn}
                                    onClick={(e) => {
                                      e.stopPropagation();
                                      history.push(
                                        `/${detailAction}/${row.id}`
                                      );
                                    }}
                                  >
                                    <VisibilityIcon />
                                  </button>
                                )}

                                {deleteAction && (
                                  <button
                                    className={styles.delete_btn}
                                    onClick={(e) => {
                                      e.stopPropagation();
                                      deleteAction(row.id);
                                    }}
                                  >
                                    <DeleteOutlinedIcon />
                                  </button>
                                )}
                              </div>
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    {emptyRows > 0 && (
                      <TableRow
                        style={{
                          height: (dense ? 33 : 53) * emptyRows,
                        }}
                      >
                        <TableCell colSpan={6} />
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component='div'
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </Paper>
            <FormControlLabel
              className={styles.Densepadding}
              control={<Switch checked={dense} onChange={handleChangeDense} />}
              label='Dense padding'
            />
          </Box>
        </main>
      </div>
    </div>
  );
}

export default EnhancedTable;
