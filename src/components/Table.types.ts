export interface Data {
    name: string;
    domain: string;
    no_of_students: number;
    no_Of_Vacancies:number;
    action: string;
  }

  export interface StudentData{
    student: string,
    year:string,
    avgPercentage: string,
    cv: string,
    action: string,
  }

export interface HeadCell {
    disablePadding: boolean;
    id: keyof Data | keyof StudentData;
    label: string;
    numeric: boolean;
  }
  
  export interface EnhancedTableProps {
    numSelected: number;
    onRequestSort: (event: React.MouseEvent<unknown>, property: keyof Data | keyof StudentData) => void;
    onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
    order: Order;
    orderBy: string;
    rowCount: number;
    tableHeader:any
  }
  export type Order = 'asc' | 'desc';

  export interface EnhancedTableToolbarProps {
    numSelected: number;
  }